= LabVIEW project documentation

Tool for automatic documentation of LabVIEW projects.

NOTE: This project is an add-on to https://gitlab.com/wovalab/open-source/labview-doc-generator[Antidoc]

== Goals

Generate documentation of a LabVIEW project.

== Documentation

The documentation is available https://wovalab.gitlab.io/open-source/docs/antidoc-addon-lvproj/latest/index.html[here].


include::CONTRIBUTING.adoc[leveloffset=+1]